#!/usr/bin/env python
from __future__ import (
    absolute_import, division, print_function,unicode_literals)
from common_functions import low_rank_approx
from common_functions import soft_threshold, dilated_threshold
from common_functions import clipped_std
from birsvd import birsvd_fast as birsvd
import numpy as np
import scipy as sci


def fitsgodec(image, method='godec',
              n_components=15, threshold_sigma=3.0, max_iter=5,
              tolerance=1.0, dilation_size=3, clip_size=3):
  """Decompose Image using soft-GoDec algorithm

  Parameters
  ----------
    image : array_like
        input image array
        0th-axis indicates time and 1st- and 2nd-axes indicat space.
    n_components : float
        number of components to compose low-rank matrix
    threshold_sigma: float
        truncating value for soft-thresholding
    tolerance : float, optional
        tolerance to quit iteration
    max_iter : int, optional
        maximum iteration number
    dilation_size : int, optional
        size of dilation disk
    clip_size : int, optional
        smallest size of sparse segments

  Returns
  -------
    L : array_like
        low-rank matrix
    S : array_like
        sparse matrix
    N : array_like
        residual matrix
    U : array_like
        low-rank basis matrix
  """
  z,y,x = image.shape
  nelem = image.size
  A = image.reshape(z,y*x)
  L = np.zeros_like(A)
  S = np.zeros_like(A)
  N = np.zeros_like(A)
  U = np.zeros_like(A)
  W = np.ones_like(A)

  for i in range(max_iter):
    L = low_rank_approx(A-S,n_components)
    N = clipped_std(A-L-S, threshold_sigma, axis=0).reshape(1,y*x)
    S = N*dilated_threshold(
          (A-L)/N, threshold_sigma, (z,y,x), dilation_size, clip_size)
    W = np.abs(S)==0
    norm = np.abs(1.0-sci.linalg.norm((A-L-S)/N)/np.sqrt(nelem))
    if (norm < tolerance): break

  if method == 'birsvd':
    L = birsvd(A-S,W,n_components)

  return L.reshape(z,y,x)
