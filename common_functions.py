#!/usr/bin/env python
from __future__ import (
    absolute_import, division, print_function,unicode_literals)
from sklearn.utils.extmath import randomized_svd
from skimage.morphology import binary_dilation
from skimage.morphology import binary_erosion
from skimage.morphology import remove_small_objects
from skimage.morphology import disk
from skimage.measure import label
import numpy as np
import warnings


def low_rank_approx(A,r):
  """Compute a low-rank approximation of a given matrix

  Parameters
  ----------
  A : array_like
      input matrix
  r : integer
      truncating rank

  Returns
  -------
  A : array_like
      a low-rank approximation of the input matrix
  """
  u,s,v = randomized_svd(A, n_components=r);
  A = u.dot(np.diag(s)).dot(v)
  return A


def soft_threshold(A,s):
  """Compute a sparse matrix by soft-thresholding

  Parameters
  ----------
  A : array_like
      input matrix
  s : float
      threshold value [s>0]

  Returns
  -------
  S : array_like
      sparse matrix
  """
  absA = np.abs(A)
  signA = np.sign(A)
  absA[absA < s] = s
  absA -= s
  return signA*absA


def dilated_threshold(A,s,dim,d=3,n=3):
  """Compute a sparse matrix by soft-thresholding

  Parameters
  ----------
  A : array_like
      input matrix
  s : float
      threshold value [s>0]
  d : integer
      dilation size [d>0]
  n : integer
      smallest size of segments [n>0]

  Returns
  -------
  S : array_like
      sparse matrix
  """
  mask = np.abs(A) > s
  with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    immask = mask.reshape(dim)
    for k in range(dim[0]):
      remove_small_objects(immask[k],n,in_place=True)
      immask[k] = binary_dilation(immask[k], disk(d))
  mask = np.logical_not(immask).reshape((dim[0],dim[1]*dim[2]))
  A[mask] = 0
  return A


def clipped_std(array, sigma, n_loop=5, axis=0):
  mask = np.ones_like(array)
  for n in range(n_loop):
    mean = np.median(array, axis=axis)
    squared = np.sum(mask*(array-mean)*(array-mean), axis=axis)
    std = np.sqrt(squared/(np.sum(mask, axis=axis)-1))
    mask = array-mean < sigma*std
  return std
