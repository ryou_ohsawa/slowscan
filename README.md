# Sample scripts for Slow Scanning

## Description
This package provides a function to subtract thermal background components from three dimensional data cube using a low-rank and sparse matrix decomposition. A decomposition algorithm is based on [GoDec][godec] and [BIRSVD][birsvd]. The algorithm provided in this package is slightly different from one in [Ohsawa et al. (2018)][slowscan], but the basic idea is the same.

``` sh
subtract source.fits output.fits
```

## Usage

``` sh
usage: subtract [-h] [--method method] [--n_components N] [--sigma_clipping s]
                [--max_iter N] [--dilation D] [--clip_size D]
                [--tolerance tol] [-f]
               input_fits output_fits

Subtract a low-rank component

positional arguments:
  input_fits          input fits cube
  output_fits         output fits image

optional arguments:
  -h, --help          show this help message and exit
  --n_components N    number of components in sparse presentation
  --sigma_clipping s  sigma clipping threshold
  --max_iter N        maximum iteration number
  --tolerance tol     tolerance threshold
  -f                  overwrite output fits image
```

## Files
- subtract: a script to subtract a low-rank component
- fitsgodec.py: provides a function to estimate a low-rank component
- common_functions.py: contains miscellaneous functions
- birsvd: a python library of Bi-Iterative Regularized Singular Value Decomposition (BIRSVD)


## Requirements
- astropy
- numpy
- scipy
- scikit-learn
- scikit-image

[godec]: https://sites.google.com/site/godecomposition/home
[birsvd]: https://www.mat.univie.ac.at/~neum/software/birsvd/
[slowscan]: http://adsabs.harvard.edu/abs/2018ApJ...857...37O
[driftscan]: http://adsabs.harvard.edu/abs/2014SPIE.9147E..9TH
